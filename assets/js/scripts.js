/** =====================
 *  LOAD JSON
 ========================*/
$(document).ready(function () {
    var content = '';
    $.getJSON('./cursos.json', function (c) {
        var items = [];
        $.each(c, function (key,val) {
            items = val;
        });
        console.log(items);
        items.forEach(function(c){
            content += '<div class="col-md-4" id="item-curso">';
            content += '<figure>';
            content += '<img class="w100" src="assets/img/cat-image.png" onclick="openModalContato();currentSlide(1)" alt="Imagem de um gato de óculos">';
            content += '</figure>';
            content += '<h2>' + c + '</h2>';
            content += '</div>';
        })
        $('#cursos').append(content);
    });

})

// Open the Modal
function openModal() {
    document.getElementById('myModal').style.display = "block";
}
function openModalContato(){
    document.getElementById('modalContato').style.display = "block";
}

// Close the Modal
function closeModal() {
    document.getElementById('myModal').style.display = "none";
}
function closeModalContato() {
    document.getElementById('modalContato').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);



function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}

